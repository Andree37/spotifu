const del = require('del');
const mustache = require('mustache');
const childProcess = require('child_process');
const mkdirp = require('mkdirp');
const fs = require('fs');
const generateClass = require('../models/generate-class');
const generateDatabase = require('../database/generate-database');
const generateApi = require('../restful-api/generate-api');
const generateBackoffice = require('../backoffice/generate-backoffice');
const generateFrontoffice = require('../frontoffice/generate-frontoffice');


function generate() {
    // removing existent directory for update
    del.sync(['Publish']);

    // creating directory for Publish
    fs.mkdir('Publish', function () {
        console.log('New directory Publish created');
    });
    mkdirp.sync('./Publish/Controllers');
    mkdirp.sync('./Publish/Models');
    mkdirp.sync('./Publish/Public/Css');
    mkdirp.sync('./Publish/Public/Images');
    mkdirp.sync('./Publish/Public/Js');
    mkdirp.sync('./Publish/Views');
    mkdirp.sync('./Publish/Schemas');
    mkdirp.sync('./Publish/Database');

    // creating index.js file on Publish
    let view = JSON.parse(fs.readFileSync('./Server/config.json'));
    fs.readFile('./Server/server.mustache', function (err, data) {
        let output = mustache.render(data.toString(), view);
        fs.writeFileSync('./Publish/index.js', output)
    });

    // copy required files
    copySchemasToPublish(view.schemas);
    copyStaticFilesToPublish(view.staticFiles);

    // generate modules
    generateClass.generate(view.dbname, view.schemas);
    generateDatabase.generate(view.dbname, view.schemas, generateDatabase.generateRelationships); //callback function for the correct construction of the database
    generateApi.generate(view.dbname, view.schemas);
    generateBackoffice.generate(view.schemas);
    generateFrontoffice.generate(view.schemas);

    // initiating server 
    childProcess.fork('./Publish/index.js');
}

function copySchemasToPublish(schemaArray) {
    schemaArray.forEach(element => {
        fs.copyFileSync(element.path, './Publish/Schemas/Schema-' + element.name + ".json");
    });
}

function copyStaticFilesToPublish(staticFiles) {
    staticFiles.forEach(element => {
        fs.copyFileSync(element.originalPath, element.destinationPath);
    });
}

module.exports.generate = generate;