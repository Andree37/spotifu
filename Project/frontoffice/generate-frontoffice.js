const mustache = require('mustache');
const fs = require('fs');
const configServer = require('../server/config.json');

function generate(schemas) {

    let importModels = [];
    schemas.forEach(element => {
        let schema = JSON.parse(fs.readFileSync(element.path));
        let title = schema['title'];
        importModels.push({
            name: title
        });
    });

    let view = {
        importModels: importModels,
        models: configServer.frontoffice
    }
    let data = fs.readFileSync('./frontoffice/frontoffice.mustache')
    let output = mustache.render(data.toString(), view);
    fs.writeFileSync('./Publish/Controllers/frontoffice.js', output);
}

module.exports.generate = generate;