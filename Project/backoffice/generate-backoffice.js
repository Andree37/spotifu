const mustache = require('mustache');
const fs = require('fs');

function generate(schemas) {

    let importModels = [];
    let routerModels = [];
    schemas.forEach(element => {
        let schema = JSON.parse(fs.readFileSync(element.path));
        let title = schema['title'];
        importModels.push({
            name: title
        });
        routerModels.push({
            name: title
        });
    });

    let view = {
        importModels: importModels,
        routerModels: routerModels
    }
    let data = fs.readFileSync('./backoffice/backoffice.mustache')
    let output = mustache.render(data.toString(), view);
    fs.writeFileSync('./Publish/Controllers/backoffice.js', output);
}

module.exports.generate = generate;