const mustache = require('mustache');
const fs = require('fs');

function generate(dbname, schemas) {

    let importModels = [];
    let functionModels = [];
    schemas.forEach(element => {
        let schema = JSON.parse(fs.readFileSync(element.path));
        let title = schema['title'];
        importModels.push({
            name: title
        });
        functionModels.push({
            name: title,
        });
    });

    let view = {
        importModels: importModels,
        functionModels: functionModels
    }
    let data = fs.readFileSync('./restful-api/api.mustache')
    let output = mustache.render(data.toString(), view);
    fs.writeFileSync('./Publish/Controllers/api.js', output)


}

module.exports.generate = generate;