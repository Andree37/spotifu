const mustache = require('mustache');
const fs = require('fs');


function generate(dbname, schemas) {

    schemas.forEach(element => {
        let schema = JSON.parse(fs.readFileSync(element.path));
        let properties = Object.keys(schema.properties);
        let references = schema.references;
        let classConstructor = [];
        let classRelationProperties = [];
        let relationMultiples = [];
        properties.forEach(function (el) {
            classConstructor.push({
                name: el
            });
        });
        let required = schema.required;
        let classEnumerables = [];
        properties.forEach(function (el) {
            if (!required.includes(el)) {
                classEnumerables.push({
                    name: el
                })
            }
        });
        if (references) {
            references.forEach(function (el) {
                if (el.relation === "M-M") {
                    relationMultiples.push({
                        lwrName: el.model.toLowerCase(),
                        firstCapName: el.model
                    });
                }
                else {
                    classConstructor.push({
                        name: el.model.toLowerCase() + "_id"
                    });
                }
            });
        }
        if (references) {
            references.forEach(function (el) {
                classRelationProperties.push({
                    lwrName: el.model.toLowerCase(),
                    firstCapName: el.model
                });
            });
        }
        //group all references
        if (references) {
            references.forEach(r => {
                properties.push(r.model.toLowerCase()+"_id");
            });
        }

        let view = {
            classTitle: element.name,
            constructorArguments: properties,
            classConstructor: classConstructor,
            classEnumerables: classEnumerables,
            relationMultiples: relationMultiples,
            classRelationProperties: classRelationProperties,
            dbname: dbname,
            primaryKey: "id",
            table: element.name,
            lwrTable: element.name.toLowerCase(),
            columns: classConstructor.map(obj => obj.name).join(),
            updateValuesParams: classConstructor.map(obj => obj.name + ' = ?').join(),
            insertValuesParams: classConstructor.map(obj => '?').join(),
            propertiesJoinThis: classConstructor.map(obj => 'this.' + obj.name).join()
        }
        let data = fs.readFileSync('./models/class.mustache')
        let output = mustache.render(data.toString(), view);
        fs.writeFileSync('./Publish/Models/' + element.name + '.js', output)
    });

}

module.exports.generate = generate;