const express = require('express');
const app = express();
const generateServer = require('./server/server');
const bodyParser = require("body-parser");
const fs = require('fs');
const mustache = require('mustache');

app.use(express.static(__dirname + '/Public/'));
app.use(bodyParser.urlencoded({
    extended: true
  }));

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/Public/index.html');
});

app.get('/editor', (req, res) => {
    res.sendFile(__dirname + '/Public/editor.html');
});

app.post('/generate', (req, res) => {
    // generate css
    let inputs = req.body;
    let view = {
        backgroundColor: inputs.backgroundColor,
        tableColor: inputs.tableColor,
        textAlign: inputs.textAlign,
        opacity: inputs.opacity,
        color: inputs.color,
        fontFamily: inputs.fontFamily
    }

    fs.readFile('./staticFiles/css.mustache', function (err, data) {
        let output = mustache.render(data.toString(), view);
        fs.writeFileSync('./Publish/Public/Css/default.css', output)
    });

    // generate server
    generateServer.generate();
    

    //respond to the request
    res.send("generated server")
});

app.listen(3000, () => console.log('App listening on port http://localhost:3000'));
// é ncessario por "label" nos schemas na zona das referencias, para saber qual é o que lista no backoffice

/**
 * req.params: array que contém os parametros associados às rotas.
 * req.body: um objeto que contém os parâmetros que são enviados no corpo do pedido. Requer que o corpo do pedido seja analisado e convertido para um formato simples de manipular, tal como faz omódulo body-parser.
 * req.headers: cabeçalhos enviados no pedido do cliente.
 * req.route: informação acerca da rota.
 * req.cookies: objeto contendo os valores de cookies enviados pelo cliente.
 * req.url, req.protocol, req.host, req.path, req.query: informação associada ao url do pedido (verfigura 1).
 * req.method: indica o verbo HTTP do pedido do cliente.
 * 
 * As principais propriedades/métodos do objeto response são:
 * res.status(código): Atualiza o código HTTP a ser enviado ao cliente. Por omissão o código é 200 (OK).
 * res.redirect([código],url): Redireciona o cliente para outra página. Por omissão, o código é 302(Found).
 * res.send(body): Envia uma resposta ao cliente cujo cabeçalho de Content-Type enviado serátext/html. Caso o argumento body enviado seja um objeto ou array ao invés de uma string, aresposta será enviada no formato JSON. Contudo para o fazer é recomendado utilizar o métodores.json.
 * res.json(json_string): Envia JSON para o cliente.
 * res.sendFile(caminho,[callback]): este método lê um ficheiro especificado pelo argumento path eenvia o seu conteúdo para o cliente.
 * res.render(view, callback): Renderiza uma view de acordo com o motor de templates que foiconfigurado no Express
 * 
 * Sobre o all
 * Alunos.all((rows) => console.log(rows));
 */