const mustache = require('mustache');
const fs = require('fs');
const sqlite3 = require('sqlite3').verbose();

function generate(dbname, schemas, relationCallback) {

    let tableConstructor = [];
    schemas.forEach(element => {
        let schema = JSON.parse(fs.readFileSync(element.path));
        let tableName = schema.title;
        let properties = schema.properties;
        let column = [];

        let types = {
            integer: "INTEGER",
            number: "REAL",
            string: "TEXT",
            date: "TEXT",
            blob: "BLOB"
        }

        Object.keys(properties).forEach((el, i, arr) => {
            schema_column = properties[el];


            let constraints = [];
            constraints = Object.keys(schema_column).filter(function (element, i , arr) {
                return (element === "pattern" && schema_column["type"] !== "date") || element === "maxLength" || element === "minimum" || element === "maximum";
            });

            column.push({
                name: el,
                type: types[schema_column.type],
                required: schema.required.includes(el) ? "NOT NULL" : "",
                unique: schema_column.required ? "REQUIRED" : "",
                // key insert here
                constraint: defineConstraint(constraints, schema_column, el),
                needComma: i !== arr.length - 1
            });
        });

        tableConstructor.push({
            name: tableName,
            tableColumns: column
        });

    });

    let view = {
        tableConstructor: tableConstructor
    }

    let data = fs.readFileSync('./database/dbscript.mustache');
    let output = mustache.render(data.toString(), view);

    let db = new sqlite3.Database("./Publish/Database/" + dbname, function (err) {
        if (err)
            return console.error(err.message);
        console.log('Connected to SQLite database.');
    });
    db.exec(output);
    db.close(function (err) {
        if (err)
            return console.error(err.message);
        console.log('Database connection closed.');
        if (relationCallback) {
            relationCallback(dbname, schemas);
        }
        
    });
}

function defineConstraint(constraints, col, colName) {

    if (constraints.length === 0) return "";

    let strConstraint = "CHECK(";
    constraints.forEach((el, i, arr) => {

        if (el === "pattern") {
            let startPos = col.pattern.indexOf('{') + 1;
            let endPos = col.pattern.indexOf('}', startPos);
            strConstraint += "LENGTH(" + colName + ") <= " + col.pattern.substring(startPos, endPos);
        } else if (el === "maxLength") {
            strConstraint += "LENGTH(" + colName + ") <= " + col.maxLength;
        } else if (el === "minimum") {
            strConstraint += colName + " >= " + col.minimum;
        } else if (el === "maximum") {
            strConstraint += colName + " <= " + col.maximum
        } 
        strConstraint += i !== arr.length - 1 ? " AND " : ""
    });
    strConstraint += ")";

    return strConstraint;
}

function generateRelationships(dbname, schemas) {
    schemas.forEach(element => {
        let schema = JSON.parse(fs.readFileSync(element.path));
        let relations = {
            "1-1": oneToOne,
            "1-M": oneToMany,
            "M-M": manyToMany
        }

        if (schema.references) {
            schema.references.forEach(relation => {
                var constraint = relations[relation.relation];
                var output = constraint(schema, relation);

                if (output) {
                    let db = new sqlite3.Database("./Publish/Database/" + dbname, function (err) {
                        if (err)
                            return console.error(err.message);
                        console.log('Connected to SQLite database.');
                    });
                    db.exec(output);
                    db.close(function (err) {
                        if (err)
                            return console.error(err.message);
                        console.log('Database connection closed.');
                    });
                }
            });
        }
    });
}

function oneToMany(schema, relation) {
    // create object of a one-to-many relation
    let output = "";

    let tableName = schema.title;
    let view = {
        tableName: tableName,
        primaryKey: "id",
        lwrForeignTable: relation.model.toLowerCase(),
        foreignTable: relation.model
    }

    let data = fs.readFileSync('./database/one-to-many.mustache');
    output = mustache.render(data.toString(), view);

    return output;
}

function manyToMany(schema, relation) {
    // create object of a many-to-many relation
    let output = "";

    // define firstName on new table
    let names = [schema.title, relation.model]
    names.sort();

    let view = {
        tableNameFirst: names[0],
        tableNameSecond: names[1],
        colNameFirstLowerCase: names[0].toLowerCase(),
        colNameSecondLowerCase: names[1].toLowerCase(),
        primaryKey: "id"
    }

    let data = fs.readFileSync('./database/many-to-many.mustache');
    output = mustache.render(data.toString(), view);

    return output;
}

function oneToOne(schema, relation) {
    // dealt like one-to-many relation, besides the creation of the unique index
    let output = oneToMany(schema, relation) + " ";

    let tableName = schema.title;

    let view = {
        indexName: relation.model.toLowerCase(),
        tableName: tableName,
        colName: "id"
    }

    let data = fs.readFileSync('./database/one-to-one.mustache');
    output += mustache.render(data.toString(), view);

    return output;
}

module.exports.generate = generate;
module.exports.generateRelationships = generateRelationships;