const sqlite3 = require('sqlite3').verbose();

/**
 * Exportar uma funcao que recebe o caminho da base de dados a ser utilizado. Quando o modulo for utilizado devera ser passado o caminho para o ficheiro da base de dados e a funÃƒÂ§ÃƒÂ£o retornarÃƒÂ¡ um objeto com 3 funÃƒÂ§ÃƒÂµes possÃƒÂ­veis: get, run e where
 * 
 * @param {any} dbpath 
 * @returns 
 */
module.exports = function (dbpath) {
    return {
        get: function (statement, params, type, callback) {
            let db = new sqlite3.Database(dbpath);
            db.get(statement, params, function (err, row) {
                console.log(row);
                if (row) {
                    row = Object.assign(new type(), row);
                }
                callback(row);
            });
            db.close();
        },
        run: async function(statement, params, callback, object, otherInserts) {
            let db = new sqlite3.Database(dbpath);
            db.run(statement, params, function (err) {
                let lastId;
                if (callback) {
                    lastId = this.lastID;
                    callback({ success: !err, error: err, rowsAffected: this.changes, lastId: this.lastID});
                }
                if (object){
                    object.id = lastId;
                }
                if (otherInserts){
                    otherInserts();
                }
                db.close();
            });
            
        },
        where: function (statement, params, type, callback) {
            let db = new sqlite3.Database(dbpath);
            db.all(statement, params, function (err, rows) {
                console.log(rows);
                rows = rows.map(function (object) {
                    return Object.assign(new type(), object);;
                });
                callback(rows);
            });
            db.close();
        }
    }
}